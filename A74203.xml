<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A74203">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties speech to the committee the 9th of March 1641 when they presented the declaration of both Houses of Parliament at New-market.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A74203 of text805 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.3[53]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A74203</idno>
    <idno type="STC">Thomason 669.f.3[53]</idno>
    <idno type="EEBO-CITATION">50811863</idno>
    <idno type="OCLC">ocm 50811863</idno>
    <idno type="VID">160611</idno>
    <idno type="PROQUESTGOID">2248533070</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A74203)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160611)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f3[53])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties speech to the committee the 9th of March 1641 when they presented the declaration of both Houses of Parliament at New-market.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Robert Barker, printer to the Kings most excellent Majesty ; and by the assignes of John Bill,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>1641 [i.e. 1642]</date>
     </publicationStmt>
     <notesStmt>
      <note>With engraving of royal seal at head of document.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- Politics and government -- 1642-1649 -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A74203</ep:tcp>
    <ep:estc>805</ep:estc>
    <ep:stc> (Thomason 669.f.3[53]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>His Majesties speech to the committee, the 9th of March, 1641. when they presented the declaration of both Houses of Parliament at New-marke</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>487</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-09</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-11</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-11</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A74203-e10">
  <body xml:id="A74203-e20">
   <pb facs="tcp:160611:1" rend="simple:additions" xml:id="A74203-001-a"/>
   <div type="speech" xml:id="A74203-e30">
    <head xml:id="A74203-e40">
     <w lemma="❧" pos="sy" xml:id="A74203-001-a-0010">❧</w>
     <w lemma="his" pos="po" xml:id="A74203-001-a-0020">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A74203-001-a-0030">Majesties</w>
     <w lemma="speech" pos="n1" xml:id="A74203-001-a-0040">Speech</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-0050">to</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-0060">the</w>
     <w lemma="committee" pos="n1" xml:id="A74203-001-a-0070">Committee</w>
     <pc xml:id="A74203-001-a-0080">,</pc>
     <w lemma="the" pos="d" xml:id="A74203-001-a-0090">the</w>
     <w lemma="9th" orig="9ᵗʰ" pos="ord" xml:id="A74203-001-a-0100">9th</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0120">of</w>
     <w lemma="march" pos="nn1" xml:id="A74203-001-a-0130">March</w>
     <pc xml:id="A74203-001-a-0140">,</pc>
     <w lemma="1641." pos="crd" xml:id="A74203-001-a-0150">1641.</w>
     <w lemma="when" pos="crq" xml:id="A74203-001-a-0160">when</w>
     <w lemma="they" pos="pns" xml:id="A74203-001-a-0170">they</w>
     <w lemma="present" pos="vvd" xml:id="A74203-001-a-0180">presented</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-0190">the</w>
     <w lemma="declaration" pos="n1" xml:id="A74203-001-a-0200">Declaration</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0210">of</w>
     <w lemma="both" pos="d" xml:id="A74203-001-a-0220">both</w>
     <w lemma="house" pos="n2" xml:id="A74203-001-a-0230">Houses</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0240">of</w>
     <w lemma="parliament" pos="n1" xml:id="A74203-001-a-0250">Parliament</w>
     <w lemma="at" pos="acp" xml:id="A74203-001-a-0260">at</w>
     <hi xml:id="A74203-e60">
      <w lemma="New-market" pos="nn1" xml:id="A74203-001-a-0270">New-market</w>
      <pc unit="sentence" xml:id="A74203-001-a-0280">.</pc>
     </hi>
    </head>
    <p xml:id="A74203-e70">
     <hi xml:id="A74203-e80">
      <w lemma="i" pos="pns" rend="decorinit" xml:id="A74203-001-a-0290">I</w>
     </hi>
     <w lemma="be" pos="vvm" xml:id="A74203-001-a-0300">Am</w>
     <w lemma="confident" pos="j" xml:id="A74203-001-a-0310">confident</w>
     <w lemma="that" pos="cs" xml:id="A74203-001-a-0320">that</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-0330">you</w>
     <w lemma="expect" pos="vvb" xml:id="A74203-001-a-0340">expect</w>
     <w lemma="not" pos="xx" xml:id="A74203-001-a-0350">not</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-0360">I</w>
     <w lemma="shall" pos="vmd" xml:id="A74203-001-a-0370">should</w>
     <w lemma="give" pos="vvi" xml:id="A74203-001-a-0380">give</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-0390">you</w>
     <w lemma="a" pos="d" xml:id="A74203-001-a-0400">a</w>
     <w lemma="speedy" pos="j" xml:id="A74203-001-a-0410">speedy</w>
     <w lemma="answer" pos="n1" xml:id="A74203-001-a-0420">Answer</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-0430">to</w>
     <w lemma="this" pos="d" xml:id="A74203-001-a-0440">this</w>
     <w lemma="strange" pos="j" xml:id="A74203-001-a-0450">strange</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-0460">and</w>
     <w lemma="unexpected" pos="j" xml:id="A74203-001-a-0470">unexpected</w>
     <w lemma="declaration" pos="n1" xml:id="A74203-001-a-0480">Declaration</w>
     <pc xml:id="A74203-001-a-0490">;</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-0500">And</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-0510">I</w>
     <w lemma="be" pos="vvm" xml:id="A74203-001-a-0520">am</w>
     <w lemma="sorry" pos="j" xml:id="A74203-001-a-0530">sorry</w>
     <pc join="right" xml:id="A74203-001-a-0540">(</pc>
     <w lemma="in" pos="acp" xml:id="A74203-001-a-0550">in</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-0560">the</w>
     <w lemma="distraction" pos="n1" xml:id="A74203-001-a-0570">Distraction</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0580">of</w>
     <w lemma="this" pos="d" xml:id="A74203-001-a-0590">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A74203-001-a-0600">Kingdom</w>
     <pc xml:id="A74203-001-a-0610">)</pc>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-0620">you</w>
     <w lemma="shall" pos="vmd" xml:id="A74203-001-a-0630">should</w>
     <w lemma="think" pos="vvi" xml:id="A74203-001-a-0640">think</w>
     <w lemma="this" pos="d" xml:id="A74203-001-a-0650">this</w>
     <w lemma="way" pos="n1" xml:id="A74203-001-a-0660">way</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0670">of</w>
     <w lemma="address" pos="n1" reg="address" xml:id="A74203-001-a-0680">Addresse</w>
     <w lemma="to" pos="prt" xml:id="A74203-001-a-0690">to</w>
     <w lemma="be" pos="vvi" xml:id="A74203-001-a-0700">be</w>
     <w lemma="more" pos="avc-d" xml:id="A74203-001-a-0710">more</w>
     <w lemma="convenient" pos="j" xml:id="A74203-001-a-0720">convenient</w>
     <pc xml:id="A74203-001-a-0730">,</pc>
     <w lemma="then" pos="av" xml:id="A74203-001-a-0740">then</w>
     <w lemma="that" pos="cs" xml:id="A74203-001-a-0750">that</w>
     <w lemma="propose" pos="vvn" xml:id="A74203-001-a-0760">proposed</w>
     <w lemma="by" pos="acp" xml:id="A74203-001-a-0770">by</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-0780">my</w>
     <w lemma="message" pos="n1" xml:id="A74203-001-a-0790">Message</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0800">of</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-0810">the</w>
     <hi xml:id="A74203-e90">
      <w lemma="20" pos="ord" xml:id="A74203-001-a-0820">20th</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0840">of</w>
     <w lemma="january" pos="nn1" reg="January" xml:id="A74203-001-a-0850">Ianuary</w>
     <w lemma="last" pos="ord" xml:id="A74203-001-a-0860">last</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-0870">to</w>
     <w lemma="both" pos="d" xml:id="A74203-001-a-0880">both</w>
     <w lemma="house" pos="n2" xml:id="A74203-001-a-0890">Houses</w>
     <pc unit="sentence" xml:id="A74203-001-a-0900">.</pc>
    </p>
    <p xml:id="A74203-e110">
     <w lemma="as" pos="acp" xml:id="A74203-001-a-0910">As</w>
     <w lemma="concerning" pos="acp" xml:id="A74203-001-a-0920">concerning</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-0930">the</w>
     <w lemma="ground" pos="n2" xml:id="A74203-001-a-0940">grounds</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-0950">of</w>
     <w lemma="your" pos="po" xml:id="A74203-001-a-0960">your</w>
     <w lemma="fear" pos="n2" xml:id="A74203-001-a-0970">Fears</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-0980">and</w>
     <w lemma="jealousy" pos="n2" reg="jealousies" xml:id="A74203-001-a-0990">Iealousies</w>
     <pc xml:id="A74203-001-a-1000">,</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-1010">I</w>
     <w lemma="will" pos="vmb" xml:id="A74203-001-a-1020">will</w>
     <w lemma="take" pos="vvi" xml:id="A74203-001-a-1030">take</w>
     <w lemma="time" pos="n1" xml:id="A74203-001-a-1040">time</w>
     <w lemma="to" pos="prt" xml:id="A74203-001-a-1050">to</w>
     <w lemma="answer" pos="vvi" xml:id="A74203-001-a-1060">answer</w>
     <w lemma="particular" pos="av-j" xml:id="A74203-001-a-1070">particularly</w>
     <pc xml:id="A74203-001-a-1080">,</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-1090">and</w>
     <w lemma="doubt" pos="vvb" xml:id="A74203-001-a-1100">doubt</w>
     <w lemma="not" pos="xx" xml:id="A74203-001-a-1110">not</w>
     <w lemma="but" pos="acp" xml:id="A74203-001-a-1120">but</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-1130">I</w>
     <w lemma="shall" pos="vmb" xml:id="A74203-001-a-1140">shall</w>
     <w lemma="do" pos="vvi" xml:id="A74203-001-a-1150">do</w>
     <w lemma="it" pos="pn" xml:id="A74203-001-a-1160">it</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-1170">to</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-1180">the</w>
     <w lemma="satisfaction" pos="n1" xml:id="A74203-001-a-1190">satisfaction</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-1200">of</w>
     <w lemma="all" pos="d" xml:id="A74203-001-a-1210">all</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-1220">the</w>
     <w lemma="world" pos="n1" xml:id="A74203-001-a-1230">world</w>
     <pc unit="sentence" xml:id="A74203-001-a-1240">.</pc>
     <w lemma="God" pos="nn1" xml:id="A74203-001-a-1250">God</w>
     <pc xml:id="A74203-001-a-1260">,</pc>
     <w lemma="in" pos="acp" xml:id="A74203-001-a-1270">in</w>
     <w lemma="his" pos="po" xml:id="A74203-001-a-1280">his</w>
     <w lemma="good" pos="j" xml:id="A74203-001-a-1290">good</w>
     <w lemma="time" pos="n1" xml:id="A74203-001-a-1300">time</w>
     <pc xml:id="A74203-001-a-1310">,</pc>
     <w lemma="will" pos="n1" xml:id="A74203-001-a-1320">will</w>
     <pc xml:id="A74203-001-a-1330">,</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-1340">I</w>
     <w lemma="hope" pos="vvb" xml:id="A74203-001-a-1350">hope</w>
     <pc xml:id="A74203-001-a-1360">,</pc>
     <w lemma="discover" pos="vvb" xml:id="A74203-001-a-1370">discover</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-1380">the</w>
     <w lemma="secret" pos="n2-j" xml:id="A74203-001-a-1390">secrets</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-1400">and</w>
     <w lemma="bottom" pos="n2" xml:id="A74203-001-a-1410">bottoms</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-1420">of</w>
     <w lemma="all" pos="d" xml:id="A74203-001-a-1430">all</w>
     <w lemma="plot" pos="n2" xml:id="A74203-001-a-1440">Plots</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-1450">and</w>
     <w lemma="treason" pos="n2" xml:id="A74203-001-a-1460">Treasons</w>
     <pc xml:id="A74203-001-a-1470">;</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-1480">and</w>
     <w lemma="then" pos="av" xml:id="A74203-001-a-1490">then</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-1500">I</w>
     <w lemma="shall" pos="vmb" xml:id="A74203-001-a-1510">shall</w>
     <w lemma="stand" pos="vvi" xml:id="A74203-001-a-1520">stand</w>
     <w lemma="right" pos="j" xml:id="A74203-001-a-1530">right</w>
     <w lemma="in" pos="acp" xml:id="A74203-001-a-1540">in</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-1550">the</w>
     <w lemma="eye" pos="n2" xml:id="A74203-001-a-1560">eyes</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-1570">of</w>
     <w lemma="all" pos="d" xml:id="A74203-001-a-1580">all</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-1590">my</w>
     <w lemma="people" pos="n1" xml:id="A74203-001-a-1600">people</w>
     <pc unit="sentence" xml:id="A74203-001-a-1610">.</pc>
     <w lemma="in" pos="acp" xml:id="A74203-001-a-1620">In</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-1630">the</w>
     <w lemma="mean" pos="j" xml:id="A74203-001-a-1640">mean</w>
     <w lemma="time" pos="n1" xml:id="A74203-001-a-1650">time</w>
     <pc xml:id="A74203-001-a-1660">,</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-1670">I</w>
     <w lemma="must" pos="vmb" xml:id="A74203-001-a-1680">must</w>
     <w lemma="tell" pos="vvi" xml:id="A74203-001-a-1690">tell</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-1700">you</w>
     <pc xml:id="A74203-001-a-1710">,</pc>
     <w lemma="that" pos="cs" xml:id="A74203-001-a-1720">That</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-1730">I</w>
     <w lemma="rather" pos="avc" xml:id="A74203-001-a-1740">rather</w>
     <w lemma="expect" pos="vvd" xml:id="A74203-001-a-1750">expected</w>
     <w lemma="a" pos="d" xml:id="A74203-001-a-1760">a</w>
     <w lemma="vindication" pos="n1" xml:id="A74203-001-a-1770">Vindication</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-1780">for</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-1790">the</w>
     <w lemma="imputation" pos="n1" xml:id="A74203-001-a-1800">Imputation</w>
     <w lemma="lay" pos="vvn" xml:id="A74203-001-a-1810">laid</w>
     <w lemma="on" pos="acp" xml:id="A74203-001-a-1820">on</w>
     <w lemma="i" pos="pno" xml:id="A74203-001-a-1830">me</w>
     <w lemma="in" pos="acp" xml:id="A74203-001-a-1840">in</w>
     <w lemma="master" pos="n1" xml:id="A74203-001-a-1850">Master</w>
     <hi xml:id="A74203-e120">
      <w lemma="pim" pos="nng1" reg="Pim's" xml:id="A74203-001-a-1860">Pims</w>
     </hi>
     <w lemma="speech" pos="n1" xml:id="A74203-001-a-1870">Speech</w>
     <pc xml:id="A74203-001-a-1880">,</pc>
     <w lemma="then" pos="av" xml:id="A74203-001-a-1890">then</w>
     <w lemma="that" pos="cs" xml:id="A74203-001-a-1900">that</w>
     <w lemma="any" pos="d" xml:id="A74203-001-a-1910">any</w>
     <w lemma="more" pos="avc-d" xml:id="A74203-001-a-1920">more</w>
     <w lemma="general" pos="j" reg="general" xml:id="A74203-001-a-1930">generall</w>
     <w lemma="rumour" pos="n2" xml:id="A74203-001-a-1940">Rumours</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-1950">and</w>
     <w lemma="discourse" pos="n2" xml:id="A74203-001-a-1960">Discourses</w>
     <w lemma="shall" pos="vmd" xml:id="A74203-001-a-1970">should</w>
     <w lemma="get" pos="vvi" xml:id="A74203-001-a-1980">get</w>
     <w lemma="credit" pos="n1" xml:id="A74203-001-a-1990">credit</w>
     <w lemma="with" pos="acp" xml:id="A74203-001-a-2000">with</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-2010">you</w>
     <pc unit="sentence" xml:id="A74203-001-a-2020">.</pc>
    </p>
    <p xml:id="A74203-e130">
     <w lemma="for" pos="acp" xml:id="A74203-001-a-2030">For</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-2040">my</w>
     <w lemma="fear" pos="n2" xml:id="A74203-001-a-2050">Fears</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-2060">and</w>
     <w lemma="doubt" pos="n2" xml:id="A74203-001-a-2070">Doubts</w>
     <pc xml:id="A74203-001-a-2080">,</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-2090">I</w>
     <w lemma="do" pos="vvd" xml:id="A74203-001-a-2100">did</w>
     <w lemma="not" pos="xx" xml:id="A74203-001-a-2110">not</w>
     <w lemma="think" pos="vvi" xml:id="A74203-001-a-2120">think</w>
     <w lemma="they" pos="pns" xml:id="A74203-001-a-2130">they</w>
     <w lemma="shall" pos="vmd" xml:id="A74203-001-a-2140">should</w>
     <w lemma="have" pos="vvi" xml:id="A74203-001-a-2150">have</w>
     <w lemma="be" pos="vvn" xml:id="A74203-001-a-2160">been</w>
     <w lemma="think" pos="vvn" xml:id="A74203-001-a-2170">thought</w>
     <w lemma="so" pos="av" xml:id="A74203-001-a-2180">so</w>
     <w lemma="groundless" pos="j" reg="groundless" xml:id="A74203-001-a-2190">groundlesse</w>
     <w lemma="or" pos="cc" xml:id="A74203-001-a-2200">or</w>
     <w lemma="trivial" pos="j" reg="trivial" xml:id="A74203-001-a-2210">triviall</w>
     <pc xml:id="A74203-001-a-2220">,</pc>
     <w lemma="while" pos="cs" xml:id="A74203-001-a-2230">while</w>
     <w lemma="so" pos="av" xml:id="A74203-001-a-2240">so</w>
     <w lemma="many" pos="d" xml:id="A74203-001-a-2250">many</w>
     <w lemma="seditious" pos="j" xml:id="A74203-001-a-2260">Seditious</w>
     <w lemma="pamphlet" pos="n2" xml:id="A74203-001-a-2270">Pamphlets</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-2280">and</w>
     <w lemma="sermon" pos="n2" xml:id="A74203-001-a-2290">Sermons</w>
     <w lemma="be" pos="vvb" xml:id="A74203-001-a-2300">are</w>
     <w lemma="look" pos="vvn" xml:id="A74203-001-a-2310">looked</w>
     <w lemma="upon" pos="acp" xml:id="A74203-001-a-2320">upon</w>
     <pc xml:id="A74203-001-a-2330">,</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-2340">and</w>
     <w lemma="so" pos="av" xml:id="A74203-001-a-2350">so</w>
     <w lemma="great" pos="j" xml:id="A74203-001-a-2360">great</w>
     <w lemma="tumult" pos="n2" xml:id="A74203-001-a-2370">Tumults</w>
     <w lemma="be" pos="vvb" xml:id="A74203-001-a-2380">are</w>
     <w lemma="remember" pos="vvn" reg="remembered" xml:id="A74203-001-a-2390">remembred</w>
     <pc xml:id="A74203-001-a-2400">,</pc>
     <w lemma="unpunished" pos="j" xml:id="A74203-001-a-2410">unpunished</w>
     <pc xml:id="A74203-001-a-2420">,</pc>
     <w lemma="uninquire" pos="vvn" xml:id="A74203-001-a-2430">uninquired</w>
     <w lemma="into" pos="acp" xml:id="A74203-001-a-2440">into</w>
     <pc xml:id="A74203-001-a-2450">:</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-2460">I</w>
     <w lemma="still" pos="av" xml:id="A74203-001-a-2470">still</w>
     <w lemma="confess" pos="vvb" reg="confess" xml:id="A74203-001-a-2480">confesse</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-2490">my</w>
     <w lemma="fear" pos="n2" xml:id="A74203-001-a-2500">Fears</w>
     <pc xml:id="A74203-001-a-2510">,</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-2520">and</w>
     <w lemma="call" pos="vvb" xml:id="A74203-001-a-2530">call</w>
     <w lemma="God" pos="nn1" xml:id="A74203-001-a-2540">God</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-2550">to</w>
     <w lemma="witness" pos="n1" reg="witness" xml:id="A74203-001-a-2560">Witnesse</w>
     <pc xml:id="A74203-001-a-2570">,</pc>
     <w lemma="that" pos="cs" xml:id="A74203-001-a-2580">That</w>
     <w lemma="they" pos="pns" xml:id="A74203-001-a-2590">they</w>
     <w lemma="be" pos="vvb" xml:id="A74203-001-a-2600">are</w>
     <w lemma="great" pos="jc" xml:id="A74203-001-a-2610">greater</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-2620">for</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-2630">the</w>
     <w lemma="true" pos="j" xml:id="A74203-001-a-2640">true</w>
     <w lemma="protestant" pos="jnn" xml:id="A74203-001-a-2650">Protestant</w>
     <w lemma="profession" pos="n1" xml:id="A74203-001-a-2660">Profession</w>
     <pc xml:id="A74203-001-a-2670">,</pc>
     <w lemma="my" pos="po" xml:id="A74203-001-a-2680">my</w>
     <w lemma="people" pos="n1" xml:id="A74203-001-a-2690">People</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-2700">and</w>
     <w lemma="law" pos="n2" xml:id="A74203-001-a-2710">Laws</w>
     <pc xml:id="A74203-001-a-2720">,</pc>
     <w lemma="then" pos="av" xml:id="A74203-001-a-2730">then</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-2740">for</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-2750">my</w>
     <w lemma="own" pos="d" xml:id="A74203-001-a-2760">own</w>
     <w lemma="right" pos="n2-j" xml:id="A74203-001-a-2770">Rights</w>
     <w lemma="or" pos="cc" xml:id="A74203-001-a-2780">or</w>
     <w lemma="safety" pos="n1" xml:id="A74203-001-a-2790">Safety</w>
     <pc xml:id="A74203-001-a-2800">;</pc>
     <w lemma="though" pos="cs" xml:id="A74203-001-a-2810">though</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-2820">I</w>
     <w lemma="must" pos="vmb" xml:id="A74203-001-a-2830">must</w>
     <w lemma="tell" pos="vvi" xml:id="A74203-001-a-2840">tell</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-2850">you</w>
     <pc xml:id="A74203-001-a-2860">,</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-2870">I</w>
     <w lemma="conceive" pos="vvb" xml:id="A74203-001-a-2880">conceive</w>
     <w lemma="that" pos="cs" xml:id="A74203-001-a-2890">that</w>
     <w lemma="none" pos="pix" xml:id="A74203-001-a-2900">none</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-2910">of</w>
     <w lemma="these" pos="d" xml:id="A74203-001-a-2920">these</w>
     <w lemma="be" pos="vvb" xml:id="A74203-001-a-2930">are</w>
     <w lemma="free" pos="j" xml:id="A74203-001-a-2940">free</w>
     <w lemma="from" pos="acp" xml:id="A74203-001-a-2950">from</w>
     <w lemma="danger" pos="n1" xml:id="A74203-001-a-2960">danger</w>
     <pc unit="sentence" xml:id="A74203-001-a-2970">.</pc>
    </p>
    <p xml:id="A74203-e140">
     <w lemma="what" pos="crq" xml:id="A74203-001-a-2980">What</w>
     <w lemma="will" pos="vmd" xml:id="A74203-001-a-2990">would</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-3000">you</w>
     <w lemma="have" pos="vvi" xml:id="A74203-001-a-3010">have</w>
     <pc unit="sentence" xml:id="A74203-001-a-3020">?</pc>
     <w lemma="have" pos="vvb" xml:id="A74203-001-a-3030">Have</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-3040">I</w>
     <w lemma="violate" pos="vvn" xml:id="A74203-001-a-3050">violated</w>
     <w lemma="your" pos="po" xml:id="A74203-001-a-3060">your</w>
     <w lemma="law" pos="n2" xml:id="A74203-001-a-3070">Laws</w>
     <pc unit="sentence" xml:id="A74203-001-a-3080">?</pc>
     <w lemma="have" pos="vvb" xml:id="A74203-001-a-3090">Have</w>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-3100">I</w>
     <w lemma="deny" pos="vvn" xml:id="A74203-001-a-3110">denied</w>
     <w lemma="to" pos="prt" xml:id="A74203-001-a-3120">to</w>
     <w lemma="pass" pos="vvi" reg="pass" xml:id="A74203-001-a-3130">passe</w>
     <w lemma="any" pos="d" xml:id="A74203-001-a-3140">any</w>
     <w lemma="one" pos="crd" xml:id="A74203-001-a-3150">one</w>
     <w lemma="bill" pos="n1" xml:id="A74203-001-a-3160">Bill</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-3170">for</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-3180">the</w>
     <w lemma="race" pos="n1" reg="race" xml:id="A74203-001-a-3190">rase</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-3200">and</w>
     <w lemma="security" pos="n1" xml:id="A74203-001-a-3210">security</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-3220">of</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-3230">my</w>
     <w lemma="subject" pos="n2" xml:id="A74203-001-a-3240">Subjects</w>
     <pc unit="sentence" xml:id="A74203-001-a-3250">?</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-3260">I</w>
     <w lemma="do" pos="vvb" xml:id="A74203-001-a-3270">do</w>
     <w lemma="not" pos="xx" xml:id="A74203-001-a-3280">not</w>
     <w lemma="ask" pos="vvi" xml:id="A74203-001-a-3290">ask</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-3300">you</w>
     <w lemma="what" pos="crq" xml:id="A74203-001-a-3310">what</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-3320">you</w>
     <w lemma="have" pos="vvb" xml:id="A74203-001-a-3330">have</w>
     <w lemma="do" pos="vvn" xml:id="A74203-001-a-3340">done</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-3350">for</w>
     <w lemma="I" pos="pno" xml:id="A74203-001-a-3360">Me</w>
     <pc unit="sentence" xml:id="A74203-001-a-3370">.</pc>
    </p>
    <p xml:id="A74203-e150">
     <w lemma="have" pos="vvb" xml:id="A74203-001-a-3380">Have</w>
     <w lemma="any" pos="d" xml:id="A74203-001-a-3390">any</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-3400">of</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-3410">my</w>
     <w lemma="people" pos="n1" xml:id="A74203-001-a-3420">People</w>
     <w lemma="be" pos="vvn" xml:id="A74203-001-a-3430">been</w>
     <w lemma="transport" pos="vvi" xml:id="A74203-001-a-3440">transport</w>
     <w lemma="with" pos="acp" xml:id="A74203-001-a-3450">with</w>
     <w lemma="fear" pos="n2" xml:id="A74203-001-a-3460">Fears</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-3470">and</w>
     <w lemma="apprehension" pos="n2" xml:id="A74203-001-a-3480">Apprehensions</w>
     <pc unit="sentence" xml:id="A74203-001-a-3490">?</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-3500">I</w>
     <w lemma="have" pos="vvb" xml:id="A74203-001-a-3510">have</w>
     <w lemma="offer" pos="vvn" xml:id="A74203-001-a-3520">offered</w>
     <w lemma="as" pos="acp" xml:id="A74203-001-a-3530">as</w>
     <w lemma="free" pos="vvi" xml:id="A74203-001-a-3540">free</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-3550">and</w>
     <w lemma="general" pos="j" reg="general" xml:id="A74203-001-a-3560">generall</w>
     <w lemma="a" pos="d" xml:id="A74203-001-a-3570">a</w>
     <w lemma="pardon" pos="n1" xml:id="A74203-001-a-3580">Pardon</w>
     <pc xml:id="A74203-001-a-3590">,</pc>
     <w lemma="as" pos="acp" xml:id="A74203-001-a-3600">as</w>
     <w lemma="yourselves" pos="pr" reg="yourselves" xml:id="A74203-001-a-3610">your selves</w>
     <w lemma="can" pos="vmb" xml:id="A74203-001-a-3630">can</w>
     <w lemma="devise" pos="vvi" xml:id="A74203-001-a-3640">devise</w>
     <pc unit="sentence" xml:id="A74203-001-a-3650">.</pc>
     <w lemma="all" pos="av-d" xml:id="A74203-001-a-3660">All</w>
     <w lemma="this" pos="d" xml:id="A74203-001-a-3670">this</w>
     <w lemma="consider" pos="vvn" xml:id="A74203-001-a-3680">considered</w>
     <pc xml:id="A74203-001-a-3690">,</pc>
     <w lemma="there" pos="av" xml:id="A74203-001-a-3700">There</w>
     <w lemma="be" pos="vvz" xml:id="A74203-001-a-3710">is</w>
     <w lemma="a" pos="d" xml:id="A74203-001-a-3720">a</w>
     <w lemma="judgement" pos="n1" reg="judgement" xml:id="A74203-001-a-3730">Iudgement</w>
     <w lemma="from" pos="acp" xml:id="A74203-001-a-3740">from</w>
     <w lemma="heaven" pos="n1" xml:id="A74203-001-a-3750">Heaven</w>
     <w lemma="upon" pos="acp" xml:id="A74203-001-a-3760">upon</w>
     <w lemma="this" pos="d" xml:id="A74203-001-a-3770">this</w>
     <w lemma="nation" pos="n1" xml:id="A74203-001-a-3780">Nation</w>
     <pc xml:id="A74203-001-a-3790">,</pc>
     <w lemma="if" pos="cs" xml:id="A74203-001-a-3800">if</w>
     <w lemma="these" pos="d" xml:id="A74203-001-a-3810">these</w>
     <w lemma="distraction" pos="n2" xml:id="A74203-001-a-3820">Distractions</w>
     <w lemma="continue" pos="vvi" xml:id="A74203-001-a-3830">continue</w>
     <pc unit="sentence" xml:id="A74203-001-a-3840">.</pc>
    </p>
    <p xml:id="A74203-e160">
     <w lemma="God" pos="nn1" xml:id="A74203-001-a-3850">God</w>
     <w lemma="so" pos="av" xml:id="A74203-001-a-3860">so</w>
     <w lemma="deal" pos="vvi" xml:id="A74203-001-a-3870">deal</w>
     <w lemma="with" pos="acp" xml:id="A74203-001-a-3880">with</w>
     <w lemma="i" pos="pno" xml:id="A74203-001-a-3890">Me</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-3900">and</w>
     <w lemma="i" pos="png" xml:id="A74203-001-a-3910">Mine</w>
     <pc xml:id="A74203-001-a-3920">,</pc>
     <w lemma="as" pos="acp" xml:id="A74203-001-a-3930">as</w>
     <w lemma="all" pos="d" xml:id="A74203-001-a-3940">all</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-3950">my</w>
     <w lemma="thought" pos="n2" xml:id="A74203-001-a-3960">Thoughts</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-3970">and</w>
     <w lemma="intention" pos="n2" xml:id="A74203-001-a-3980">Intentions</w>
     <w lemma="be" pos="vvb" xml:id="A74203-001-a-3990">are</w>
     <w lemma="upright" pos="av-j" xml:id="A74203-001-a-4000">upright</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-4010">for</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4020">the</w>
     <w lemma="maintenance" pos="n1" xml:id="A74203-001-a-4030">maintenance</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-4040">of</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4050">the</w>
     <w lemma="true" pos="j" xml:id="A74203-001-a-4060">true</w>
     <w lemma="protestant" pos="jnn" xml:id="A74203-001-a-4070">Protestant</w>
     <w lemma="profession" pos="n1" xml:id="A74203-001-a-4080">Profession</w>
     <pc xml:id="A74203-001-a-4090">,</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-4100">and</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-4110">for</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4120">the</w>
     <w lemma="observation" pos="n1" xml:id="A74203-001-a-4130">Observation</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-4140">and</w>
     <w lemma="preservation" pos="n1" xml:id="A74203-001-a-4150">Preservation</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-4160">of</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4170">the</w>
     <w lemma="law" pos="n2" xml:id="A74203-001-a-4180">Laws</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-4190">of</w>
     <w lemma="this" pos="d" xml:id="A74203-001-a-4200">this</w>
     <w lemma="land" pos="n1" xml:id="A74203-001-a-4210">Land</w>
     <pc xml:id="A74203-001-a-4220">:</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-4230">And</w>
     <pc xml:id="A74203-001-a-4240">,</pc>
     <w lemma="i" pos="pns" xml:id="A74203-001-a-4250">I</w>
     <w lemma="hope" pos="vvb" xml:id="A74203-001-a-4260">hope</w>
     <pc xml:id="A74203-001-a-4270">,</pc>
     <w lemma="God" pos="nn1" xml:id="A74203-001-a-4280">God</w>
     <w lemma="will" pos="vmb" xml:id="A74203-001-a-4290">will</w>
     <w lemma="bless" pos="vvi" reg="bless" xml:id="A74203-001-a-4300">blesse</w>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-4310">and</w>
     <w lemma="assist" pos="vvi" xml:id="A74203-001-a-4320">assist</w>
     <w lemma="those" pos="d" xml:id="A74203-001-a-4330">those</w>
     <w lemma="law" pos="n2" xml:id="A74203-001-a-4340">Laws</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-4350">for</w>
     <w lemma="my" pos="po" xml:id="A74203-001-a-4360">my</w>
     <w lemma="preservation" pos="n1" xml:id="A74203-001-a-4370">preservation</w>
     <pc unit="sentence" xml:id="A74203-001-a-4380">.</pc>
    </p>
    <p xml:id="A74203-e170">
     <w lemma="as" pos="acp" xml:id="A74203-001-a-4390">As</w>
     <w lemma="for" pos="acp" xml:id="A74203-001-a-4400">for</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4410">the</w>
     <w lemma="Additionall" pos="j" xml:id="A74203-001-a-4420">Additionall</w>
     <w lemma="declaration" pos="n1" xml:id="A74203-001-a-4430">Declaration</w>
     <pc xml:id="A74203-001-a-4440">,</pc>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-4450">you</w>
     <w lemma="be" pos="vvb" xml:id="A74203-001-a-4460">are</w>
     <w lemma="to" pos="prt" xml:id="A74203-001-a-4470">to</w>
     <w lemma="expect" pos="vvi" xml:id="A74203-001-a-4480">expect</w>
     <w lemma="a" pos="d" xml:id="A74203-001-a-4490">an</w>
     <w lemma="answer" pos="n1" xml:id="A74203-001-a-4500">Answer</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-4510">to</w>
     <w lemma="it" pos="pn" xml:id="A74203-001-a-4520">it</w>
     <pc xml:id="A74203-001-a-4530">,</pc>
     <w lemma="when" pos="crq" xml:id="A74203-001-a-4540">when</w>
     <w lemma="you" pos="pn" xml:id="A74203-001-a-4550">you</w>
     <w lemma="shall" pos="vmb" xml:id="A74203-001-a-4560">shall</w>
     <w lemma="receive" pos="vvi" xml:id="A74203-001-a-4570">receive</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4580">the</w>
     <w lemma="answer" pos="n1" xml:id="A74203-001-a-4590">Answer</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-4600">to</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4610">the</w>
     <w lemma="declaration" pos="n1" xml:id="A74203-001-a-4620">Declaration</w>
     <w lemma="itself" pos="pr" reg="itself" xml:id="A74203-001-a-4630">it self</w>
     <pc unit="sentence" xml:id="A74203-001-a-4650">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A74203-e180">
   <div type="colophon" xml:id="A74203-e190">
    <p xml:id="A74203-e200">
     <pc xml:id="A74203-001-a-4660">¶</pc>
     <w lemma="imprint" pos="vvn" xml:id="A74203-001-a-4670">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A74203-001-a-4680">at</w>
     <w lemma="London" pos="nn1" xml:id="A74203-001-a-4690">London</w>
     <w lemma="by" pos="acp" xml:id="A74203-001-a-4700">by</w>
     <w lemma="Robert" pos="nn1" xml:id="A74203-001-a-4710">Robert</w>
     <w lemma="Barker" pos="nn1" xml:id="A74203-001-a-4720">Barker</w>
     <pc xml:id="A74203-001-a-4730">,</pc>
     <w lemma="printer" pos="n1" xml:id="A74203-001-a-4740">Printer</w>
     <w lemma="to" pos="acp" xml:id="A74203-001-a-4750">to</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4760">the</w>
     <w lemma="king" pos="n2" xml:id="A74203-001-a-4770">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A74203-001-a-4780">most</w>
     <w lemma="excellent" pos="j" xml:id="A74203-001-a-4790">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A74203-001-a-4800">Majesty</w>
     <pc xml:id="A74203-001-a-4810">:</pc>
     <w lemma="and" pos="cc" xml:id="A74203-001-a-4820">And</w>
     <w lemma="by" pos="acp" xml:id="A74203-001-a-4830">by</w>
     <w lemma="the" pos="d" xml:id="A74203-001-a-4840">the</w>
     <w lemma="assign" pos="vvz" reg="assigns" xml:id="A74203-001-a-4850">Assignes</w>
     <w lemma="of" pos="acp" xml:id="A74203-001-a-4860">of</w>
     <w lemma="JOHN" pos="nn1" xml:id="A74203-001-a-4870">JOHN</w>
     <w lemma="bill" pos="nn1" xml:id="A74203-001-a-4880">BILL</w>
     <pc unit="sentence" xml:id="A74203-001-a-4890">.</pc>
     <w lemma="1641." pos="crd" xml:id="A74203-001-a-4900">1641.</w>
     <pc unit="sentence" xml:id="A74203-001-a-4910"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
